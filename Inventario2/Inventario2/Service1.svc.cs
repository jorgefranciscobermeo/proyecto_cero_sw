﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Inventario2
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        Inventario2Entities db = new Inventario2Entities();

        public List<categoria> BuscarCategorias()
        {

            return db.categoria.ToList();

        }

        public categoria BuscarCategoria(string id)
        {
            int nId = Convert.ToInt32(id);
            return db.categoria.Where(p => p.id_ctg == nId).First();
        }

        public void InsertarCategoria(string id, string nom)
        {

            int z = Convert.ToInt32(id);

            categoria model = new categoria
            {

                id_ctg = z,
                nom_ctg = nom
            };

            db.categoria.Add(model);
            db.SaveChanges();



        }

        public void ModificarCategoria(string id, string nom)
        {

            int z = Convert.ToInt32(id);
            var mod = (from m in db.categoria where m.id_ctg == z select m).First();
            mod.nom_ctg = nom;
            db.SaveChanges();

        }


        public void EliminarCategoria(string id)
        {

            int z = Convert.ToInt32(id);
            var del = (from d in db.categoria where d.id_ctg == z select d).First();
            db.categoria.Remove(del);
            db.SaveChanges();
        }











        //EMPRESA


        public List<empresa> BuscarEmpresas()
        {

            return db.empresa.ToList();

        }


        public List<empresa> BuscarEmpresa(string id)
        {
            List<empresa> emp = new List<empresa>();
            int z = Convert.ToInt32(id);
            var a = (from d in db.empresa where d.id_emp == z select d).First();
            emp.Add(a);

            return emp;
        }



        public void InsertarEmpresa(string id, string nom, string tel, string dir, string ema)
        {

            int z = Convert.ToInt32(id);

            empresa model = new empresa
            {
                id_emp = z,
                nom_emp = nom,
                tel_emp = tel,
                dir_emp = dir,
                ema_emp = ema

            };

            db.empresa.Add(model);
            db.SaveChanges();

        }


        public void ModificarEmpresa(string id, string nom, string tel, string dir, string ema)
        {

            int z = Convert.ToInt32(id);
            var mod = (from m in db.empresa where m.id_emp == z select m).First();
            mod.nom_emp = nom;
            mod.tel_emp = tel;
            mod.dir_emp = dir;
            mod.ema_emp = ema;
            db.SaveChanges();

        }





        public void EliminarEmpresa(string id)
        {

            int z = Convert.ToInt32(id);
            var del = (from d in db.empresa where d.id_emp == z select d).First();
            db.empresa.Remove(del);
            db.SaveChanges();



        }


        //PRODUCTO


        public List<producto> BuscarProductos()
        {

            return db.producto.ToList();

        }


        public List<producto> BuscarProducto(string id)
        {
            List<producto> emp = new List<producto>();
            int z = Convert.ToInt32(id);
            var a = (from d in db.producto where d.id_pdt == z select d).First();
            emp.Add(a);

            return emp;
        }



        public void InsertarProducto(string id, string nom, string pre, string can)
        {

            int z = Convert.ToInt32(id);

            producto model = new producto
            {
                id_pdt = z,
                nom_pdt = nom,
                pre_pdt = pre,
                can_pdt = can,


            };

            db.producto.Add(model);
            db.SaveChanges();

        }


        public void ModificarProducto(string id, string nom, string pre, string can)
        {

            int z = Convert.ToInt32(id);
            var mod = (from m in db.producto where m.id_pdt == z select m).First();
            mod.nom_pdt = nom;
            mod.pre_pdt = pre;
            mod.can_pdt = can;
            db.SaveChanges();

        }





        public void EliminarProducto(string id)
        {

            int z = Convert.ToInt32(id);
            var del = (from d in db.producto where d.id_pdt == z select d).First();
            db.producto.Remove(del);
            db.SaveChanges();

        }

        // Proveedor


        public List<proveedor> BuscarProveedores()
        {

            return db.proveedor.ToList();

        }


        public List<proveedor> BuscarProveedor(string id)
        {
            List<proveedor> emp = new List<proveedor>();
            int z = Convert.ToInt32(id);
            var a = (from d in db.proveedor where d.id_pvd == z select d).First();
            emp.Add(a);

            return emp;
        }



        public void InsertarProveedor(string id, string nom, string tel, string dir, string ema)
        {

            int z = Convert.ToInt32(id);

            proveedor model = new proveedor
            {
                id_pvd = z,
                nom_pvd = nom,
                tel_pvd = tel,
                dir_pvd = dir,
                ema_pvd = ema

            };

            db.proveedor.Add(model);
            db.SaveChanges();

        }


        public void ModificarProveedor(string id, string nom, string tel, string dir, string ema)
        {

            int z = Convert.ToInt32(id);
            var mod = (from m in db.proveedor where m.id_pvd == z select m).First();
            mod.nom_pvd = nom;
            mod.tel_pvd = tel;
            mod.dir_pvd = dir;
            mod.ema_pvd = ema;
            db.SaveChanges();

        }





        public void EliminarProveedor(string id)
        {

            int z = Convert.ToInt32(id);
            var del = (from d in db.proveedor where d.id_pvd == z select d).First();
            db.proveedor.Remove(del);
            db.SaveChanges();



        }


    }
}

