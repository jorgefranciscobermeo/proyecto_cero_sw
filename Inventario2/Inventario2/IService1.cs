﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Inventario2
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarCategorias", ResponseFormat = WebMessageFormat.Json)]
        List<categoria> BuscarCategorias();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarCategoria/{id}", ResponseFormat = WebMessageFormat.Json)]
        categoria BuscarCategoria(string id);


        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "InsertarCategoria?id=1&nom=abc", ResponseFormat = WebMessageFormat.Json)]
        void InsertarCategoria(string id, string nom);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarCategoria/{id}/{nom}", ResponseFormat = WebMessageFormat.Json)]
        void ModificarCategoria(string id, string nom);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarCategoria/{id}", ResponseFormat = WebMessageFormat.Json)]
        void EliminarCategoria(string id);


        //EMPRESA


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarEmpresas", ResponseFormat = WebMessageFormat.Json)]
        List<empresa> BuscarEmpresas();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarEmpresa/{id}", ResponseFormat = WebMessageFormat.Json)]
        List<empresa> BuscarEmpresa(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "InsertarEmpresa/{id}/{nom}/{tel}/{dir}/{ema}", ResponseFormat = WebMessageFormat.Json)]
        void InsertarEmpresa(string id, string nom, string tel, string dir, string ema);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarEmpresa/{id}/{nom}/{tel}/{dir}/{ema}", ResponseFormat = WebMessageFormat.Json)]
        void ModificarEmpresa(string id, string nom, string tel, string dir, string ema);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarEmpresa/{id}", ResponseFormat = WebMessageFormat.Json)]
        void EliminarEmpresa(string id);


        //PRODUCTO

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarProductos", ResponseFormat = WebMessageFormat.Json)]
        List<producto> BuscarProductos();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarProducto/{id}", ResponseFormat = WebMessageFormat.Json)]
        List<producto> BuscarProducto(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "InsertarProducto/{id}/{nom}/{pre}/{can}", ResponseFormat = WebMessageFormat.Json)]
        void InsertarProducto(string id, string nom, string pre, string can);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarProducto/{id}/{nom}/{pre}/{can}", ResponseFormat = WebMessageFormat.Json)]
        void ModificarProducto(string id, string nom, string pre, string can);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarProducto/{id}", ResponseFormat = WebMessageFormat.Json)]
        void EliminarProducto(string id);


        //PROVEEDOR

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarProveedores", ResponseFormat = WebMessageFormat.Json)]
        List<proveedor> BuscarProveedores();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarProveedor/{id}", ResponseFormat = WebMessageFormat.Json)]
        List<proveedor> BuscarProveedor(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "InsertarProveedor/{id}/{nom}/{tel}/{dir}/{ema}", ResponseFormat = WebMessageFormat.Json)]
        void InsertarProveedor(string id, string nom, string tel, string dir, string ema);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarProveedor/{id}/{nom}/{tel}/{dir}/{ema}", ResponseFormat = WebMessageFormat.Json)]
        void ModificarProveedor(string id, string nom, string tel, string dir, string ema);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarProveedor/{id}", ResponseFormat = WebMessageFormat.Json)]
        void EliminarProveedor(string id);

        

    }



    
}
