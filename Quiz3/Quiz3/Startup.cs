﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Quiz3.Startup))]
namespace Quiz3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
