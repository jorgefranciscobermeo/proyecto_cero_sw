﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Quiz3.Models;

namespace Quiz3.Controllers
{
    public class UsuarioController : Controller
    {
        private Model1Container db = new Model1Container();

        // GET: /Usuario/
        public ActionResult Index()
        {
            var usuarioset = db.USUARIOSet.Include(u => u.PERSONA);
            return View(usuarioset.ToList());
        }

        // GET: /Usuario/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USUARIO usuario = db.USUARIOSet.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // GET: /Usuario/Create
        public ActionResult Create()
        {
            ViewBag.PERSONAID_PERSONA = new SelectList(db.PERSONASet, "ID_PERSONA", "NOMBRE1_PERSONA");
            return View();
        }

        // POST: /Usuario/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="NOMBRE_USUARIO,PASSWORD_USUARIO,ULTIMA_CONEXION,TOTAL_CONEXION,PERSONAID_PERSONA")] USUARIO usuario)
        {
            if (ModelState.IsValid)
            {
                db.USUARIOSet.Add(usuario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PERSONAID_PERSONA = new SelectList(db.PERSONASet, "ID_PERSONA", "NOMBRE1_PERSONA", usuario.PERSONAID_PERSONA);
            return View(usuario);
        }

        // GET: /Usuario/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USUARIO usuario = db.USUARIOSet.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            ViewBag.PERSONAID_PERSONA = new SelectList(db.PERSONASet, "ID_PERSONA", "NOMBRE1_PERSONA", usuario.PERSONAID_PERSONA);
            return View(usuario);
        }

        // POST: /Usuario/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="NOMBRE_USUARIO,PASSWORD_USUARIO,ULTIMA_CONEXION,TOTAL_CONEXION,PERSONAID_PERSONA")] USUARIO usuario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PERSONAID_PERSONA = new SelectList(db.PERSONASet, "ID_PERSONA", "NOMBRE1_PERSONA", usuario.PERSONAID_PERSONA);
            return View(usuario);
        }

        // GET: /Usuario/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            USUARIO usuario = db.USUARIOSet.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: /Usuario/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            USUARIO usuario = db.USUARIOSet.Find(id);
            db.USUARIOSet.Remove(usuario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
