
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/19/2015 09:02:10
-- Generated from EDMX file: C:\Users\jorgef\Documents\Visual Studio 2013\sw2\Quiz3\Quiz3\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [nueva];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'PERSONASet'
CREATE TABLE [dbo].[PERSONASet] (
    [ID_PERSONA] int IDENTITY(1,1) NOT NULL,
    [NOMBRE1_PERSONA] nvarchar(max)  NOT NULL,
    [NOMBRE2_PERSONA] nvarchar(max)  NOT NULL,
    [APELLIDO1_PERSONA] nvarchar(max)  NOT NULL,
    [APELLIDO2_PERSONA] nvarchar(max)  NOT NULL,
    [ESTATURA_PERSONA] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'USUARIOSet'
CREATE TABLE [dbo].[USUARIOSet] (
    [NOMBRE_USUARIO] int IDENTITY(1,1) NOT NULL,
    [PASSWORD_USUARIO] nvarchar(max)  NOT NULL,
    [ULTIMA_CONEXION] nvarchar(max)  NOT NULL,
    [TOTAL_CONEXION] nvarchar(max)  NOT NULL,
    [PERSONAID_PERSONA] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID_PERSONA] in table 'PERSONASet'
ALTER TABLE [dbo].[PERSONASet]
ADD CONSTRAINT [PK_PERSONASet]
    PRIMARY KEY CLUSTERED ([ID_PERSONA] ASC);
GO

-- Creating primary key on [NOMBRE_USUARIO] in table 'USUARIOSet'
ALTER TABLE [dbo].[USUARIOSet]
ADD CONSTRAINT [PK_USUARIOSet]
    PRIMARY KEY CLUSTERED ([NOMBRE_USUARIO] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [PERSONAID_PERSONA] in table 'USUARIOSet'
ALTER TABLE [dbo].[USUARIOSet]
ADD CONSTRAINT [FK_PERSONAUSUARIO]
    FOREIGN KEY ([PERSONAID_PERSONA])
    REFERENCES [dbo].[PERSONASet]
        ([ID_PERSONA])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PERSONAUSUARIO'
CREATE INDEX [IX_FK_PERSONAUSUARIO]
ON [dbo].[USUARIOSet]
    ([PERSONAID_PERSONA]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------